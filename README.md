本项目 fork 项目go_spider，github：https://github.com/hu17889/go_spider ，因此项目架构的部分文档可以参考此项目。

同时项目架构、部分思路参考了 python 的经典爬虫scrapy。

基本结构：

	Spider模块（主控）
	Downloader模块（下载器）
	PageProcesser模块（页面分析）
	History（Url采集历史记录）
	Scheduler模块（任务队列）
	Pipeline模块（结果输出）

主要Feature:

    1. 基于Go语言的并发采集
    2. 页面下载、分析、持久化模块化，可自定义扩展
    3. 采集日志记录（Mongodb支持）
    4. 页面数据自定义存储（Mysql、Mongodb）
    5. 深度遍历，同时可自定义深度层次
    6. Xpath解析
    
主要用于定向采集网站。

后续开发拟支持下列特性：

    1. 多类型采集目标支持，例如图片下载，文档下载
    2. 网页JavaScript渲染、数据生成支持，页面分析集成浏览器内核
	3. 网页抽取规则可配置
	4. 登陆采集
	5. 采集User Agent控制，采集频率控制，防反扒
	6. Scheduler 调度支持Redis或者Message Queue
	7. 显示分布式采集：任务调度，资源控制

ps：dense spider 瓦屋密蛛，四川境内发现的世界上最小的蜘蛛

所有依赖：

	go get github.com/PuerkitoBio/goquery
	go get github.com/bitly/go-simplejson
	go get golang.org/x/net/html/charset
	go get github.com/go-xorm/xorm
	go get github.com/go-sql-driver/mysql
	go get gopkg.in/mgo.v2
	go get gopkg.in/mgo.v2/bson
	xmlpath 其中xmlpath已经整理到项目中

示例执行：

    1.下载代码
    2.编译：go install dense_spider/example/cnblogs_page_processor
    3.执行：./bin/cnblogs_page_processor