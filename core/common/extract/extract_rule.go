package extract

type ExtractRule struct {
	name  string
	xpath string
}

func NewExtractRule(name string, xpath string) ExtractRule {
	return ExtractRule{name: name, xpath: xpath}
}
func (this *ExtractRule) GetName() string {
	return this.name
}
func (this *ExtractRule) GetXpath() string {
	return this.xpath
}
